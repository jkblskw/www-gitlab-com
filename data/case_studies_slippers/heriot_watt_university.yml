title: Heriot Watt University
cover_image: /images/blogimages/cover_image_heriot_watt_uni.jpg
cover_title: |
  How GitLab automates course work, enhances coding feedback, advances software quality, and scales for success.
cover_description: |
  Professors and students leverage GitLab to enhance and enable better feedback on coding assignments, to improve learning and code quality, and boost students’ programming experience.
canonical_path: /customers/heriot_watt_university/
customer_logo: /images/case_study_logos/hwlogo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Education
customer_location: Edinburgh, Scotland, UK
customer_solution: GitLab SaaS Ultimate
customer_employees:  |
      29,000
customer_overview: |
  Software engineering skills are critical to the modern economy, but learning the essential concepts and best practices of programming is not getting easier. Within Heriot-Watt University’s Computer Science department, GitLab is being used to drive formative feedback, and improve coding skills for students as they prepare to enter the industry.
  
customer_challenge: |
  The Heriot-Watt University Computer Science department looked to automate portions of code assignment reviews, individualize students’ learning, provide version control, improve formative feedback, and scale-up software development courses to support additional campuses in Asia and beyond.

key_benefits: >-


    Scaled-up support for multiple campuses as school launches international satellite campus expansion program


    Equipping students with state-of-the art agile tooling and preparing them for ever-higher levels of challenge in software engineering


    More immediate feedback on coding efforts and overall improvement in communications with shorter feedback loops


    Open source culture aligns with the University ethos and values
 

customer_stats:
  - stat: 10x 
    label: Improvement in marking times with support of automated tests
  - stat: |
        24,290     
    label: Project forks by 1796 users
  - stat: 15
    label: Coding-based courses employing GitLab over four Heriot-Watt campuses


customer_study_content:
  - title: the customer
    subtitle: Heriot-Watt University Computer Science Department turbocharges old learning methods
    content: >-
    

        Heriot-Watt University is dedicated to equipping students to enter the high-technology workforce prepared to achieve. The Heriot-Watt Computer Science Department is consistently scored among the top computer science departments in the UK for teaching and research capabilities.  A big part of the department’s mission is taking an innovative approach to programming skills development.
    

  - title: the challenge
    subtitle:  Overcoming barriers to successful programming education
    content: >-
    

        The path to insightful and efficient learning is challenging for teaching teams and students alike. That became even more vivid during the recent global COVID-19 pandemic, which suddenly required scalable agile systems supporting extensive remote learning.In 2018, Heriot-Watt teaching teams were at crossroads. They faced barriers on their quest to improve important programming exercises and looked for a solution to provide students with state-of-the-art tools. At the time, starter-code and completed exercises were distributed and contributed via ZIP files. Lecturers extracted files and inspected coding exercises manually. 
    

        Archiving of student submissions was an ad-hoc responsibility for students. In addition, peer interactions were limited solely to face-to-face lab sessions, while email was a principal means of communication. Teaching teams were in need of an intuitive, robust, formative feedback loop mechanism to nurture learning of best coding practices. 
    
   
  - title: the solution
    subtitle:  GitLab creates power peer testing
    content: >-
    

        For the HWU Computer Science Department, the evaluation process began with the use of highly agile GitLab threaded-discussion capabilities. Next came steps toward systematic testing of student code. Progressive use of elements in the complete GitLab ecosystem was important, as the Heriot-Watt Computer Science Department endeavored to support work at additional remote campuses located across the globe.
    

        The ability to self-host GitLab enables students to use their Heriot-Watt University IT accounts with GitLab, via OAuth authentication. To facilitate lab helper interactions and course deployment, Heriot-Watt University has developed an open source [Haskell library](https://hackage.haskell.org/package/gitlab-haskell) for writing GitLab programs and type safe GitLab system hooks. Heriot-Watt University uses it to automate commonly performed GitLab actions when delivering coding courses, which provides a consistent student experience across their courses at a global scale.
    

        Lecturers are able to monitor incremental completion of student assignments and, in turn, identify common misunderstandings amongst learners. GitLab use allows the teaching teams to bring the “commit early, commit often” philosophy of today’s coder ranks to the classroom.GitLabs continuous integration supports automated testing of student code, reducing the time it takes to mark submissions. Heriot-Watt University has an intensive software development track in years 1 and 2, in which GitLab is extensively integrated to ensure students gain the programming skills required for higher-level specialized courses including AI, distributed systems, programming language constructions, and security. 
         
  - title: the results
    subtitle: Improving student programmer outcomes, adapting to pandemic challenge
    content: >-
    

        GitLab has been embraced by the Heriot-Watt student programming community, and adoption has even seen students creating their own GitLab projects for group work and their own coursework. Rather than shuffling through emails, students use managed GitLab thread discussions with lecturers and their peers with a clearer 'close to the code' view of feedback, along with contextual information.
    

        GitLab has played an instrumental role in preparing students for professional practice, especially in terms of learning about source control, builds and best practices in agile development. GitLab is a great platform to introduce version control to students, especially as it closely mirrors the commercial repository hosts. GitLab has become central to equipping early year students with vital skills for advanced final year courses. A recent survey of Heriot-Watt students indicated that over 86% enjoyed using GitLab in their courses.
    

        Naturally, many will encounter GitLab upon entering their careers. Their HWU experience prepares them for careers where skills working with this repository host is invaluable. Using GitLab trains them in the ways of modern programming and modern DevOps. With ready access to issues, storyboards, pull requests and job runners, students are exposed to agile methodologies, development best practices, and tools for automatic testing and deployment.
    

        Perhaps most notably, the students gain experience in collaborative development, sharing expertise, and code critiques. This happens as a natural part of the learning/development process, within a common programming environment. Moreover, student users cite GitLab’s pivotal role in enabling collaboration during a pandemic-driven lockdown, when they could not gather together in the same place. “It allowed us to take online coursework and submit and receive programming exercises in a way I did not know was possible, allowing for the continuation of studies during the [COVID-19 Pandemic] lockdown.” Jérémy Bruyère, MSc Artificial Intelligence student. 
    

        Students receive feedback that would be otherwise hard to get efficiently. Peer groups are formed for formative feedback supporting anonymity between peers. HWU teaching team members report that using peer testing in the GitLab environment positively impacts students’ understanding of the role of testing in modern development. GitLab provides a dependable platform and in its essential support of open-source software, it directly aligns with HWU values.
    

        GitLab has been central to improvement of code quality and documentation practices among Heriot-Watt University Computer Science students, and it has enhanced the effectiveness of critical peer testing practices that prepare students to handle real-world applications. “GitLab has made group work easier…it was a crucial tool in multiple group work projects. Really helped with coordinating development”, added Manuel Maarek, Computer Science Lecturer, HWU. The open source culture of community and transparency at GitLab aligns with the University ethos and approach. 
    

        The Heriot-Watt University Computer Science program, with the help of GitLab, has worked to create a dynamic dialogue between students and instructors - in particular, to close the feedback loop for programming exercises, improve the efficiency of students when completing lab work, and increase code quality thanks to peer feedback and peer testing approaches embedded into Heriot-Watt University's computer science teaching practices.
    


    

        ## Learn more about GitLab solutions
    

        [Value Stream Management with GitLab](/solutions/value-stream-management/)
    

        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    

        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: GitLab has played an instrumental role in preparing students for professional practice, especially in terms of learning about source control, builds and best practices in agile development.
    attribution: Rob Stewart
    attribution_title: Assistant Professor of Computer Science, Heriot Watt University





