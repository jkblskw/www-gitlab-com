title: Join the GitLab for Open Source Program
description: GitLab wants to give back to the open source community by helping
  teams be more efficient, secure, and productive. Learn more here!
file_name: join
parent_solution: open-source
twitter_image: /images/opengraph/open-source-card.png
header:
  title: Join the GitLab for Open Source Program
  subtitle: Our top tiers, and 50,000 CI minutes, are free for Open Source projects
  image: /images/solutions/open-source-partners-logo.svg
tracks:
  heading: About
  body_copy: >-
    GitLab exists today in large part thanks to the work of hundreds of thousands of open source contributors around the world. To give back to this community, the [GitLab for Open Source program](https://about.gitlab.com/solutions/open-source/) was created to help open source teams be more efficient, secure, and productive by allowing them to use GitLab's [top tier](https://about.gitlab.com/pricing/) capabilities.


    [Public projects](https://docs.gitlab.com/ee/public_access/public_access.html) on GitLab.com do not automatically receive top tier functionality at the project level. Apply to this program if you need top tier functionality at the project/group level or if your open source project will need the additional CI minutes that come with this program.

  card:
    - heading: A few things to keep in mind as you apply
      body: >-
        * Free top tier accounts do not include [support](https://about.gitlab.com/support/).

        * Your program membership needs to be renewed annually. If you do not renew, [your account will be downgraded](https://about.gitlab.com/pricing/licensing-faq/#what-happens-when-my-subscription-is-about-to-expire-or-has-expired).

        * Acceptance into the GitLab for Open Source Program is at GitLab's sole discretion, and we reserve the right to terminate the Program, or change the [Program requirements](https://about.gitlab.com/solutions/open-source/join/#requirements), at any time.

    - heading: GitLab for Open Source Agreement
      icon_full: /images/icons/slp-sourcecode.svg
      body: >-
            Upon acceptance, all program members are subject to the [GitLab for Open Source Program Agreement](https://about.gitlab.com/handbook/legal/opensource-agreement/).

    - heading: Requirements
      icon_full: /images/icons/slp-lock.svg
      body: >-

        In order to be accepted into the GitLab for Open Source Program, applicants must meet the following requirements:


        * **OSI-approved open source license:** All of the code you host in this GitLab group must be published under [OSI-approved open source licenses](https://opensource.org/licenses/category)

        * **Not seeking profit:** Your organization must not seek to make a profit through services or by charging for higher tiers. Accepting donations to sustain your efforts is ok. [Read more about this requirement here](https://about.gitlab.com/handbook/marketing/community-relations/opensource-program/#who-qualifies-for-the-gitlab-for-open-source-program).

        * **Publicly visible:** Your GitLab.com group or self-managed instance *and* your source code must be [publicly visible and publicly available](https://docs.gitlab.com/ee/public_access/public_access.html).


        If you believe your project meets all program requirements, we welcome your application!


        Note: Unfortunately, we are not able to accept all open source projects that are affiliated with the US Federal government. If you are affiliated, please let us know and we will work with you to see if your project qualifies.
    - heading: Program Benefits
      icon_full: /images/icons/slp-agile.svg
      body: >-

        * Unlimited seats per license of our top-tier functionality (SaaS or self-managed). The number of seats is the number of different users that will use this license during the next year. For more information, see the FAQ below.


        * The number of seats and type of license (Saas or self-managed) can be changed at the time of renewal or upon request.


        * 50k CI runner minutes are included with the subscription. ([Additional minutes must be purchased](https://docs.gitlab.com/ee/subscriptions/#purchasing-additional-ci-minutes)).


        If you have any additional questions regarding this program, feel free to reach us at [opensource@gitlab.com](mailto:opensource@gitlab.com).

block_list_long:
  heading: Frequently Asked Questions
  items_left:
    - text: Do I have to apply for every OSS project separately?
      body: The subscription is going to be for a group, so only one application will
        be enough. Apply with your most representative project.
      dropdown: true
    - text: Can I use this license for my non-open source projects, or to host private
        projects as well?
      body: No. The GitLab license can be used only for the publicly visible open
        source projects it was approved for. In some cases, we allow program
        members to host a small number of private projects if they have
        sensitive data in them. Please email us at opensource@gitlab.com to ask
        about your use case. You must obtain written permission by us to use the
        license outside of the program requirements.
      dropdown: true
    - text: Is GitLab available in my country?
      dropdown: true
      body: >-
        GitLab is available to people all over the world in [many languages](https://translate.gitlab.com/<). However, since we are a U.S.-based company, we do not offer our services in [U.S. embargoed countries](/handbook/business-ops/trade-compliance/#effect-on-customer-facing-web-pages).
    - text: Why can't I submit my form?
      dropdown: true
      body: If you live in Ukraine, you'll need to verify that you do not live in the
        Crimean Penninsula, since Crimea is on the U.S. embargo list. If you
        list your country as "Ukraine" our form won't automatically let you
        submit. If you do not live in Crimea, please fill out the application
        form and add your full address in the "billing address" section. Provide
        your city in the city drop down, and choose a different country (not
        Ukraine) in the country drop-down. You should now be able to submit the
        form.
    - text: Is it possible to increase the number of seats in the future?
      body: If you wish to increase the number of seats on your existing license,
        please send an email to opensource@gitlab.com, and we’ll prepare an
        add-on quote for additional seats.
      dropdown: true
  items_right:
    - body: Sign up for a Free account (can be hosted or self-managed), or enroll in a
        free trial for our top tiers. Create your group and set its visibility
        to Public. If you're using a self-managed instance, make sure it is
        publicly accessible. Add OSI-approved license information to at least
        one of the projects in your group's LICENSE files
      dropdown: true
      text: "How do I create a qualifying group or project on GitLab? "
    - body: We recommend that you renew your license at least one month in advance in
        case there are any delays. If your license is about to expire, here's
        what to expect.
      text: "What happens when my subscription is about to expire or has expired? "
      dropdown: true
    - body: Please see our licensing and subscription FAQ for the detailed
        explanation.
      text: "Who gets counted in the subscription? "
      dropdown: true
    - body: A GitLab.com subscription uses a concurrent (seat) model, which refers to
        the maximum number of users enabled at once. Every occupied seat is
        counted, including the owner's, with the exception of members with Guest
        permissions. You can learn more about roles and permissions here. We are
        happy to grant you the number of seats you think you'll need for current
        project members as well as additional ones to plan for growth during the
        year. You'll have the opportunity to request additional seats at time of
        renewal, or you can email us at opensource@gitlab.com to request more
        during the year.
      text: How do I choose the correct number of seats for my project?
      dropdown: true
    - body: Some web browsers and privacy extensions block the application form
        because they identify it as a pop-up. Please disable the pop-up blocker
        on this page, refresh the page, and check again.
      text: "Why can't I see an application form on this page? "
      dropdown: true
    - body: While all public projects hosted on GitLab.com already have access to top
        tier features at the project level, this does not include group-level
        Ultimate features like roadmaps, epics, and more.  The GitLab for Open
        Source license provides the additional ability to use top tier
        group-level features. It also allows projects to have 50,000 CI minutes
        per month for free.
      text: "What are the benefits of joining the GitLab for Open Source program? "
      dropdown: true
markto:
  heading: Application
  alert: >-
    Applications will not be processed during U.S. Holidays and responses may be delayed during those periods. Furthermore, a few extra steps are required before we transition to a more automated application process. We appreciate your patience during this transition. To help us  prioritize, please let us know if your request is urgent by including
    "Urgent: \[your request]" in the subject line of any email to
    opensource@gitlab.com.
  body: >-
    ### Application process


    * **Before you begin:**

    * **Set up a GitLab account.** You'll need to have a GitLab group already set up for your open source project or organization via a Free account or Free Trial. For help setting up a GitLab group, please see the relevant [FAQ below](https://about.gitlab.com/solutions/open-source/join/#faqs). If you are considering a migration, you do not need to have finished the migration before applying to our program. You will need at least one project (repo) set up under your GitLab group to comply with our requirements.

    * **Take screenshots.** During the application process, you'll need to provide [3 screenshots](https://docs.gitlab.com/ee/subscriptions/#gitlab-for-open-source) of your project. We suggest taking them in advance, since you'll need to submit them on page two of the application form.

    * **Once you're ready to apply:**

    * **Fill out the form.** Submit the application form on the right side of this page to help us verify that you meet all program requirements.

    * **Wait time.** Gitlab uses SheerID, a trusted partner, to verify that you meet the GitLab for Open Source requirements. In most cases, you can expect a decision about your application within 15 business days. You may be asked to provide additional information.


    ### What to expect


    After you complete the application form, if you are verifed, we'll work with you to grant your license. Here's how:


    * **Coupon code.** You will recieve an email with instructions and a one time unique coupon code.

    * **Terms and Conditions.** Once you fill out the licensing form, you will then recieve a zero dollar quote with our [terms and conditions](https://about.gitlab.com/handbook/legal/opensource-agreement/) for signature.

    * **Customer Portal.** Directions for accessing your GitLab for Open Source license will be sent after the signed quote is accepted. The license key (self-managed) or group authentication (SaaS) will require you to log in to the [GitLab Customer Portal](https://customers.gitlab.com/).


  form:
    code: >-
      <iframe src="https://offers.sheerid.com/gitlab/member/" width="800"
      height="1300" ></iframe>
                  </figure>
body_block_2:
  header: Renewal
  content: >-
    We are currently automating the application process in order to provide our community with the best experience possible. As we roll out the automation project in phases, a few extra steps are required and you may experience delays. We appreciate your patience during this transition. To help us prioritize, please let us know if your request is urgent by including "Urgent: [your request]" in the subject line of any email to opensource@gitlab.com.


    ### Email a renewal request


    You will need to email us annually to renew your membership.


    Please begin the renewal process **at least one month in advance** to ensure sufficient processing time. You will receive email reminders to do so and can renew as early as three months in advance of your membership's expiration.


    To renew, please send an email to [opensource@gitlab.com](mailto:opensource@gitlab.com) with the following information (copy and paste):

      <pre>    `Subject: Renewal Request | Date of Expiration (MM/YYYY)

          To help us find your account:
          *   Name of your organization or project
          *   Email associated with this account
          To help us make sure you still qualify:
          *   Link to your publicly visible GitLab instance
          *   Link to one of your OSI-compliant licenses
          *   Written acceptance of this statement (Include this sentence in your request): `I confirm that my organization does not seek to make a profit from this OSS project`
          To help us plan for next year:
          *   Number of seats you are renewing.
          *   Any change of ownership to the account.
          *   If the ownership needs to change, please send the new account holder's name, email address, and contact's mailing address.`
      </pre>


    Our program requirements may change from time to time, and we'll need to make sure that you continue to meet them year after year. If you do not qualify upon renewal, we'll work with you to make any needed transition as smooth as possible.


    If you do not renew your membership for any reason, [your account will be downgraded](https://about.gitlab.com/pricing/licensing-faq/#what-happens-when-my-subscription-is-about-to-expire-or-has-expired).


    ### What to expect


    If your renewal request is accepted, you'll be asked to sign a zero dollar renewal quote. After that, here's what to expect for each type of renewal:

    *   **Saas renewals:** No further action is necessary after your renewal is fully processed. Your account will automatically renew.

    *   **Self-managed renewals:** You'll need to download your license from the [GitLab Customer Portal](https://customers.gitlab.com) and upload it to your instance.


    If you need help at any point, [we are here to support you](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs).
