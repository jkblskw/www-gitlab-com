---
layout: handbook-page-toc
title: "Objectives and Key Results (OKRs)"
description: "OKRs stand for Objective-Key Results and are our quarterly objectives. OKRs are how to achieve the goal of the Key Performance Indicators KPIs."
canonical_path: "/company/okrs/"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Most recent OKRs

All our OKRs are public and listed on the pages below.
- [FY23-Q1 Active](/company/okrs/fy23-q1/){:data-ga-name="fy23-q1"}{:data-ga-location="body"}
- [FY23-Q2 Future](/company/okrs/fy23-q2/){:data-ga-name="fy23-q2"}{:data-ga-location="body"}
- [Previous OKRs](#okr-archive){:data-ga-name="okr archive"}{:data-ga-location="body"}

## What are OKRs?

[OKRs](https://en.wikipedia.org/wiki/OKR) stand for Objectives and Key Results and are our quarterly objectives.
OKRs are _how_ to achieve the goal of the Key Performance Indicators [KPIs](/handbook/ceo/kpis/){:data-ga-name="KPIs"}{:data-ga-location="body"}.
They lay out our plan to execute our [strategy](/company/strategy/){:data-ga-name="strategy"}{:data-ga-location="body"} and help make sure our goals and how to achieve that are clearly defined and aligned throughout the organization.
The **Objectives** help us understand *what* we're aiming to do,
and the **Key Results** help paint the picture of *how* we'll measure success of the objective.
You can use the phrase “We will achieve a certain OBJECTIVE as measured by the following KEY RESULTS…” to know if your OKR makes sense.
The OKR methodology was pioneered by Andy Grove at Intel and has since helped align and transform companies around the world.

OKRs have four superpowers:
* Focus
* Alignment
* Tracking
* Stretch

We do not use it to [give performance feedback](/handbook/people-group/360-feedback/){:data-ga-name="give performance feedback"}{:data-ga-location="body"} or as a [compensation review](/handbook/total-rewards/compensation/){:data-ga-name="compensation review"}{:data-ga-location="body"} for team members.

The [E-Group](/company/team/structure/#e-group){:data-ga-name="e-group"}{:data-ga-location="body"} does use it for their [Performance Enablement Reviews](/handbook/people-group/learning-and-development/career-development/#performance-enablement-review){:data-ga-name="performance enablement reviews"}{:data-ga-location="body"}

The [Chief of Staff to the CEO](/job-families/chief-executive-officer/chief-of-staff/){:data-ga-name="chief of staff"}{:data-ga-location="body"} initiates and guides the OKR process.

Watch EVP, Engineering Eric Johnson discuss the power of OKRs from his perspective:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/aT66up3SyVU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### OKRs are stretch goals by default

OKRs should be ambitious but achievable. If you achieve less than 70% of your KR, it may have not been achievable. If you are regularly achieving 100% of your KRs, your goals may not be ambitious enough.

Some KRs will measure new approaches or processes in a quarter. When this happens, it can be difficult to determine what is ambitious and achievable because we lack experience with this kind of measurement. For these first iterations, we prefer to set goals that seem ambitious and expect a normal distribution of high, medium, and low achievement across teams with this KR.

### Shared Objectives

If there is something important that requires two (or more) parts of our organization, all leaders involved should share the same or similar objective. They should have deconflicted key results so they can still achieve things within their sphere of control. This is in keeping with our concepts of [collaboration](/handbook/values/#collaboration){:data-ga-name="collaboration"}{:data-ga-location="body"} and [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/#what-is-a-directly-responsible-individual){:data-ga-name="DRI"}{:data-ga-location="body"}.

### OKRs are what is different

The OKRs are what initiatives we are focusing on this quarter specifically.
Our most important work are things that happen every quarter.
Things that happen every quarter are measured with [Key Performance Indicators](/handbook/ceo/kpis){:data-ga-name="key performance indicators"}{:data-ga-location="body"}.
Part of the OKRs will be or cause changes in KPIs.

### Pass-thru Key Results

It's acceptable for managers and reports to have an identical key result. For instance, something really important might need to happen at the executive level, but it's a manager or IC several layers apart who is doing the actual execution. Every person in that line of reporting should have the same key result.

While it can feel like double-counting, it is consistent with [Andy Grove's](https://en.wikipedia.org/wiki/Andrew_Grove) concept of Managerial Leverage outlined in his book [High Output Management](https://www.amazon.com/High-Output-Management-Andrew-Grove/dp/0679762884). This ensures that conversations happen in the relevant 1:1's, that everyone knows the latest status, and that the person executing does not accidentally get re-tasked. Please remember to recognize the person that achieved the result so there is no perception of "taking credit" for others' work.

### Target Dates in Key Results

Just because quarters are a useful timeframe for objectives, does not mean that key results should default to being due on the last day of the quarter. This could lead to unnecessary delays. Consider putting specific target dates into the key result phrase to indicate when it is needed by.

You should decide your scoring methodology ahead of time. You might score an OKR as 0% if it misses its target date. Or, if it's less time sensitive, you might penalize it 10% for each week it's delayed.

### How do I prioritize OKRs in the light of other priorities?

OKRs do not replace or supersede core team member responsibilities or performance indicators. OKRs are additive and are essentially a high signal request from your leadership team to prioritize the work. They typically are used to help galvanize the entire company or a set of teams to rapidly move in the one direction together. They are expected to be completed unless you have higher priority work that is surfaced and agreed to by leadership.  When surfacing tradeoffs, team members should not factor in how an unmet OKR may impact your executive leadership bonus in their prioritization. They should instead focus on GitLab priorities. If your executive leader still feels that the OKR is more important, they will ask you to *disagree and commit*. 

## Ally.io for OKRs

As of FY23-Q2, team members will use a third-party vendor, [Ally.io](https://ally.io/), for OKRs. While GitLab's Plan functionality is great for project management, it currently has limitations around cascading initiatives between teams and aggregated scoring. Ally.io will provide team members with a single source of truth for OKR tracking and reporting. Individuals and teams can still choose to operationalize their OKRs within issues and epics.

All team members have access to Ally.io. New team members are automatically imported from BambooHR within a week of their joining. Access your Ally.io account by going to your Okta dashboard and clicking on your Ally tile or adding it as a tile, if you do not see it as one of your Apps.

### Ally.io is not limited access

Since all team members have access to information in Ally.io, it is a home for OKR details that are both [internal only](/handbook/communication/#internal) and public. [Limited access details](/handbook/communication/#limited-access) should not be captured here. 

## Who sets OKRs?

Generally, we do OKRs up to the team level.
As a company, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/), but some functions may.
For example, in the Engineering Division Staff-level (and above) individual contributors have OKRs. However, it is not required of Staff Product Designers at this time.
Also, individual contributors in the Engineering Division who are not required to do OKRs are welcome to do them with their manager. It's a useful way to prepare for a managerial career, or to align one's activities with the broader goals of the company.

An individual might also have OKRs if they represent a unique team.
For example, individual SDRs don't have OKRs, the SDR team does.
If Legal is one person but represents a unique function, Legal has OKRs.
Part of the individual performance review is the answer to: how much did this person contribute to the team objectives?

## OKR Process at GitLab

The EBA to the CEO is responsible for scheduling and coordination of the OKRs process detailed below.
Scheduling should be completed at least 30 days in advance of the beginning of the OKR process, which begins 5 weeks before the start of the [fiscal quarter](/handbook/finance/#fiscal-year){:data-ga-name="fiscal quarter"}{:data-ga-location="body"}.

Should you need to reschedule, please @ mention the EBA to the CEO in the `#eba-team` slack channel. 

### CEO initiates new quarter's OKRs

**Six Mondays** before the start of the fiscal quarter, the CEO and Chief of Staff to the CEO initiates the OKR process.

The CoS to the CEO creates a Google Doc for E-Group alignment and shares initial suggestions with the CEO. The CEO and CoS to the CEO discuss and modify these initial suggestions. This document is shared with E-Group in the [E-Group Weekly](/handbook/e-group-weekly/) that is **five weeks** before the start of the coming quarter. E-Group is encouraged to offer feedback in the E-Group Weekly, directly within the Google Doc, or in meetings with the CEO or CoST.

**Four Mondays** before the staret of the quarter, the CoS to the CEO will add the CEO OKRs to Ally.io. The CoS to the CEO will then post the Ally.io link in the #okrs Slack channel and @ mention the CEO. After the CEO confirms the post and makes any edits, the CoS to the CEO will @ mention e-group and exec-admins on the Slack thread.

CEO OKRs may continue to be refined in the lead up to the coming quarter.

### Executives propose OKRs for their functions

**Four Mondays** before the start of the fiscal quarter, in the days after the CEO shares OKRs with all of GitLab in the #okr channel, Executives propose OKRs for their functions via Ally.io through a Slack message in #okrs channel. The CEO and Chief of Staff to the CEO should be @ mentioned. The CEO will confirm sign-off on objectives through commenting directly in them. While the CEO is the DRI, this responsibility may be the delegated to the CoS to the CEO.

Each executive should aim for a maximum of 3 objectives. Each objective has between 1 and 3 key results; if you have less, you list less. While OKRs are known for being ambitious or committed, we only have ambitious OKRs. When drafting the OKRs, executives should strive to have clear numerical targets.

Function objectives should cascade from one of the CEO's OKR's in Ally.io.

Executives should consider how their OKR efforts can have the greatest impact on the organization. Functions can have objectives under any of the three CEO OKRs. For example, the People Team could have an objective under the CEO's Net ARR OKR if it identified that a specific enablement activity were key to driving sales or the Sales Team could have an objective under the CEO's Great Teams OKR if it were focused on improving team diversity. Functions should not be pigeonholed into the CEO OKR that appears to be most directly related to the function.

When ready for review or when changes to objectives or KRs are made, relevant objectives and KR links from Ally.io should be shared in the #okrs channel in Slack and at-mention the Chief of Staff to the CEO and CEO. The CEO is responsible for OKR approvals, but may delegate this responsibility to the CoS to the CEO. 

Function objective should cascade from one the CEO's OKRs in Ally.io using the steps below:
OKR owners should author new OKRs in their corresponding Department/Sub-department/Compartment/Team in Ally.io.
   1. In Ally, navigate to your Team by selecting **All Teams** from the lefthand side. If you do not see your team, contact the CoST.
   1. Verify you are in your Team and in the relevant Fiscal Quarter, and click **Add an Objective.**
   1. Enter the title of the Objective. 
   1. Ensure that only one [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) is assigned to the OKR/KR. If there is a case of multi-ownership, it's likely that the OKR/KR can be simplified or broken down further.

### OKR Draft Review Meeting

The week that begins **three Mondays** before the start of the fiscal quarter, there is an OKR Draft Review Meeting for the e-group. This meeting is an opportunity for executives to get feedback from one another and highlight any dependencies on other functions to one another.
The agenda for this meeting is structured as follows:
1. Function
   1. Link to the objective in Ally.io
   1. Dependencies: call out any dependencies

If additional action needs to be taken by the functional leader, the Ally.io link should be re-shared in the #okrs channel in Slack when it's ready for final review.

### Cascade!

Now that Executive (function-level) OKRs are set
(as set as things are at GitLab; Everything is always in Draft!),
Executives shift their focus to finalizing OKRs to their team.

This is also the opportunity to create team OKRs in Ally.io (objectives and key results) and add them to the relevant CEO and executive OKR.


### Dependency Commitments

Top level dependencies should be identified in the [OKR Draft Review Meeting](https://about.gitlab.com/company/okrs/#okr-draft-review-meeting){:data-ga-name="OKR draft review"}{:data-ga-location="body"}, but it is likely that additional dependencies will be identified as OKRs cascade. We want to avoid situations where big asks are coming weeks into the quarter, or teams are being committed to do work without first having input. This makes it difficult for teams to manage their own priorities and succeed in their own initiatives. 

It is each team's responsibility to proactively identify dependencies in which the team cannot reach success without meaningful engagement from another team. In these instances, it is important that all teams required to make a significant contribution sign off on the KR and agree on the level of support to be provided. A [boring solution](/handbook/values/#boring-solutions) is to create a sister KR for other departments that will need to be actively involved and link the KRs using the dependency function. KRs with dependencies should not be considered final until other teams have confirmed support, but other teams should also respect department KRs as the top priorities for the overall business and do what they can to support. 

### Documenting How to Achieve

In FY21, we had quarterly How to Achieve Meetings in which senior team members shared their plans for achieving KRs. These meetings were often short and inefficient as much of the content could be covered during the planning process and reviewed async. While we no longer have these meetings, plans for achieving KR targets should still be clearly documented within Ally.io or issues and epics that are linked to from Ally.io. Ally.io entries should include the following fields:

1. **Overview:** some additional background on what we are trying to accomplish and why it is important. This section should elaborate on KRs that are not fully descriptive and provide context for team members who might not otherwise understand the desired result.
1. **DRI or Core Team**: document the [DRI](/handbook/people-group/directly-responsible-individuals/){:data-ga-name="DRI"}{:data-ga-location="body"}. Other key team members and their roles can also be captured in this section.
1. **Action Items:** a list of 3-10 key action items for achieving the target. You can consider using check boxes or a chart to help communicate progress.
1. **Scoring**: your KR should be a statement that clearly indicates how you will score final achievement at the end of quarter. If it is, this section is not needed. When how the KR will be scored isn't immediately clear from the KR itself, details on how scoring will work should be documented according to [scoring guidelines](/okrs/#scoring-okrs){:data-ga-name="OKRs"}{:data-ga-location="body"}. 

### The quarter begins

The Chief of Staff to the CEO takes CEO OKRs and updates the OKR handbook page for the current quarter to be active. Each objective and KR should include the related Ally.io link. The CoST for the CEO should also create the handbook page for the following quarter and document the OKR process timeline. 

The CoS to the CEO shares the handbook update MR in the #okr channel in Slack and @ mentioned e-group. .

### Making changes within quarter

We value [iteration](/handbook/values/#iteration){:data-ga-name="iteration"}{:data-ga-location="body"}. We can change an objective or KR if we find that in our pursuit of the initial KR we have not set the optimal goal. Here are a few examples of when this could happen:

1. It becomes clear that the performance indicator does not provide the best measure for success
1. A KR statement exists, but the target is a placeholder. For example, "Obtain $XM in bookings in Q1"
1. A KR is related to achievement of a new inititiave, and it is agreed that we should pivot in terms of scope or focus as we learn more about what we want to achieve

Please note that iteration **does not** mean [changing or lower goal posts](https://about.gitlab.com/handbook/values/12-things-that-are-not-iteration){:data-ga-name="changing or lower goal posts"}{:data-ga-location="body"}, because it looks like we can't meet what were ambitious but agreed upon key results.

It is better to update an objective or KR than continue to work toward a goal that is not best aligned with desired business [results](/handbook/values/#results){:data-ga-name="results"}{:data-ga-location="body"}. In instances where CEO KRs are being updated in the spirit of iteration, flag the Ally.io change in the #okrs Slack channel and tag the CEO and Chief of Staff to the CEO and the CEO for approval. Approval of the change indicates that the revised goal has been agreed upon. At this point, you can also update any associated issues and epics that exist.

In the event that a functional objective that is captured in Ally.io needs to be updated, please note the change in the #okrs Slack channel and tag the CEO and Chief of Staff to the CEO for approval. Approval of the change indicates that the revised goal has been agreed upon.


## Format of OKR on the Handbook Page

Top level CEO KRs will appear in the handbook. OKRs have numbers attached to them for [ease of reference, not for ranking](/handbook/communication/#numbering-is-for-reference-not-as-a-signal){:data-ga-name="ease of reference"}{:data-ga-location="body"}. In order to maintain a [single source of truth](/handbook/documentation/#documentation-is-the-single-source-of-truth-ssot){:data-ga-name="single source of truth"}{:data-ga-location="body"}, starting in Q2 FY23, we're putting functional objectives and KRs in Ally.io and linking this to the handbook page. We made this change, because Ally.io enhances our ability to draw connections between and cascade OKRs throughout GitLab. It also provides a SSoT for OKRs. We were also struggling with merge conflicts as regular changes were being made to the handbook page. 

Functional leaders are responsible for updating the their objectives and KRs in Ally before each [Key Review](/handbook/key-review/#automated-kpi-slides){:data-ga-name="key reviews"}{:data-ga-location="body"}.


#### Objectives

The objectives for the quarter are defined in the [OKR section of the handbook](https://about.gitlab.com/company/okrs/){:data-ga-name="OKR section of the handbook"}{:data-ga-location="body"}. 
To add new objectives in Ally.io, follow the steps below:
1. In Ally.io, click the add objective button. A dialogue box will open to allow you to enter key information.
1. Enter a short but despriptive title for the objective.
1. Identify and select the type of objective: individual, team or orgnaization.
1. Identify the owner for the objective.
1. Identify the time period or quarter for the objective. 
1. Outcome: Add the outcome for the objective, this will automatically be set to 100%. The outcomes added can be measured in % or numeric.
1. Click create and the objective will be added.

#### Key Results

Each [Objective](#objectives) will contain one or more Key Result issues. 
To add new key results in Ally.io, follow the steps below:
1. Find the objective that you created and want to add a key result. 
1. On the right, hover over the more actions and select 'add key result', this will open a dialogue box to populate. 
1. Enter a descriptive title for the key result.
1. Check that the type, owner and time period are correct. 
1. Add the outcome for the key results, outcome can either be measures in % or numeric. 
1. Click create and the key results will appear under the corresponding objective. 

Watch this video for how to add objectives and key results and align them with CEO OKRs: 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/HAQ6cXNAOEM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


#### Tips and Tricks
* [How to comment on an objective and key result](https://youtu.be/YajA4xOgdD0)
* [How to add dependencies](https://youtu.be/6XzWlNMH440). Dependencies allow a team member to tag any OKR to other OKRs within Ally. This could be used when there is an OKR that requires a component of another OKR to be completed for success. 

## Ally.io Resources
* Learn the essential features of Ally.io with [Ally.io Software Essentials](https://learn.ally.io/ally-software-essentials) (16 mins) 
* [Ally Help Center](https://help.ally.io/en/) 


## Maintaining the status of OKRs

Teams should update the Health Status of their KR issues and present them in their [Key Review](/handbook/key-review/){:data-ga-name="key reviews"}{:data-ga-location="body"}.
When presenting the status of OKRs, we use the following terms to denote the status of a key result:
1. On track - the DRI is confident the key result will be achieved.
1. Needs attention - the DRI believes there is some risk the key result will be achieved. Elevated attention is required in order for the key result to be achieved.
1. At risk - the DRI does not expect the key result will be achieved. Urgent action is required in order for the key result to be achieved.

A Key Results health status should be maintained as the SSOT on the status. This is something that should be able to be referenced at any point in order to get a clear view of progress against the objective. The objective will roll up the health statuses of all relevant KRs. 

During Key Reviews, teams should include material that covers [key OKR progress  details](/handbook/key-review/#okr-slides){:data-ga-name="okr slides"}{:data-ga-location="body"} and links to relevant OKRs.

The first Key Review of the following quarter should offer a clear scoring for each KR.

## Scoring OKRs

Your KRs should be statements that clearly indicate how you will score. For example, in FY21-Q4, the marketing team set a target of completing 5 experiments. It completed 4 out of the 5, but only one of these appeared to be successful. The marketing team initially saw this as a failure. Instead, it showed notable progress. 80% of experiments were completed. This was the stated KR goal. 

We should aspire to set quantitative goals in which scoring is straightforward as a % of attainment (for example, X% of target ARR or logos). In rare instances, quantitative KRs are not possible or appropriate. For example, launch a new compensation program is hard to score without scoring guidelines. Does not launching = 0% attainment and launching = 100% attainment? What about hitting milestones in between? In these cases, note in the issue or epic how you plan to grade the KR at the end of quarter. In our compensation example, this could mean putting together a scoring guide such as this:

| Milestone | Score |
| ------ | ------ |
| Complete compensation analysis | 20% |
| Present plan to E-Group for feedback | 40% |
| Get sign-off from Finance | 60% |
| Complete comp refresh for all team members | 100% |

Please update scores in addition to status in Key Result Meetings.

## Everyone can contribute

Everyone is welcome to a suggestion to improve any OKR.
To update please make a merge request and post a link to the MR in the #okrs channel in Slack and at-mention the Chief of Staff to the CEO. If commenting on a functional objective or KR, comment directly in the epic or issue.

## OKR resources:
* [With Goals, FAST beats SMART](https://sloanreview.mit.edu/article/with-goals-fast-beats-smart/)
* [Measure What Matters by John Doerr](https://www.whatmatters.com)
* [A Modern Guide to Lean OKRs](https://worldpositive.com/a-modern-guide-to-lean-okrs-part-i-c4a30dba5fa1)

## OKR Archive
- [FY22-Q4](/company/okrs/fy22-q4/){:data-ga-name="fy22-q4"}{:data-ga-location="body"}
- [FY22-Q3](/company/okrs/fy22-q3/){:data-ga-name="fy22-q3"}{:data-ga-location="body"}
- [FY22-Q2](/company/okrs/fy22-q2/){:data-ga-name="fy22-q2"}{:data-ga-location="body"}
- [FY22-Q1](/company/okrs/fy22-q1/){:data-ga-name="fy22-q1"}{:data-ga-location="body"}
- [FY21-Q4](/company/okrs/fy21-q4/){:data-ga-name="fy21-q4"}{:data-ga-location="body"}
- [FY21-Q3](/company/okrs/fy21-q3/){:data-ga-name="fy21-q3"}{:data-ga-location="body"}
- [FY21-Q2](/company/okrs/fy21-q2/){:data-ga-name="fy21-q2"}{:data-ga-location="body"}
- [FY21-Q1](/company/okrs/fy21-q1/){:data-ga-name="fy21-q1"}{:data-ga-location="body"} 
- [FY20-Q4](/company/okrs/fy20-q4/){:data-ga-name="fy20-q4"}{:data-ga-location="body"}
- [FY20-Q3](/company/okrs/fy20-q3/){:data-ga-name="fy20-q3"}{:data-ga-location="body"}
- [FY20-Q2](/company/okrs/fy20-q2/){:data-ga-name="fy20-q2"}{:data-ga-location="body"}
- [FY20-Q1](/company/okrs/fy20-q1/){:data-ga-name="fy20-q1"}{:data-ga-location="body"}
- [CY18-Q4](/company/okrs/2018-q4/){:data-ga-name="2018-q4"}{:data-ga-location="body"}
- [CY18-Q3](/company/okrs/2018-q3/){:data-ga-name="2018-q3"}{:data-ga-location="body"}
- [CY18-Q2](/company/okrs/2018-q2/){:data-ga-name="2018-q2"}{:data-ga-location="body"}
- [CY18-Q1](/company/okrs/2018-q1/){:data-ga-name="2018-q1"}{:data-ga-location="body"}
- [CY17-Q4](/company/okrs/2017-q4/){:data-ga-name="2017-q4"}{:data-ga-location="body"}
- [CY17-Q3](/company/okrs/2017-q3/){:data-ga-name="2017-q3"}{:data-ga-location="body"}
